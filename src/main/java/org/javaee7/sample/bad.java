package org.javaee7.sample;

public class Bad {
    public bad() {
        int x = 5;
        int y = 10;
        
        // Duplicated logic block 1
        int z = x + y;
        System.out.println("Sum is: " + z);
        System.out.println("Sum is: " + z); // Duplicated line
        
        // Duplicated logic block 2
        int a = 3;
        int b = 7;
        int c = a + b;
        System.out.println("Sum is: " + c);
        System.out.println("Sum is: " + c); // Duplicated line
    }
}
